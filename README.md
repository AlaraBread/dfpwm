# DFPWM

more information and downloads available at [alarabread.fun](https://alarabread.fun/plugins)

## Building

After installing [Rust](https://rustup.rs/), you can compile DFPWM as follows:

```shell
cargo xtask bundle dfpwm --release
```
