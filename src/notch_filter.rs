use std::f32::consts::TAU;

pub struct NotchFilter {
	a: f32,
	k: f32,
	w: f32,
	z_1: f32,
	z_2: f32,
}

impl NotchFilter {
	pub fn new(f: f32) -> Self {
		// we aren't calculating w using sample rate because we
		// are filtering out a frequency that scales with the sample rate.
		let w: f32 = f*TAU;
		let a: f32 = 0.999;
		let mut n = Self {
			a,
			k: 1.0,
			w,
			z_1: 0.0,
			z_2: 0.0,
		};
		n.set_a(a);
		return n;
	}

	pub fn set_a(&mut self, a: f32) {
		self.a = a;
		self.k = (1.0+2.0*a*self.w.cos()+a*a)/(2.0+2.0*self.w.cos());
	}

	pub fn set_f(&mut self, f: f32) {
		self.w = f*TAU;
		self.set_a(self.a);
	}

	pub fn reset(&mut self) {
		self.z_1 = 0.0;
		self.z_2 = 0.0;
	}

	pub fn process_sample(&mut self, input: f32) -> f32 {
		let out = input+self.z_1;
		self.z_1 = out*2.0*self.a*self.w.cos()-input*2.0*self.w.cos()+self.z_2;
		self.z_2 = input-out*self.a*self.a;
		return out*self.k;
	}
}