pub struct DFPWMCompress {
	q: f32,
	s: f32,
	lt: f32,
	pub prec: f32,
}

impl DFPWMCompress {
	pub fn new() -> Self {
		Self {
			q: 0.0,
			s: 0.0,
			lt: -1.0,
			prec: 12.28,
		}
	}

	pub fn reset(&mut self) {
		self.q = 0.0;
		self.s = 0.0;
		self.lt = -1.0;
	}
	
	pub fn process_sample(&mut self, input: f32) -> bool {
		let prec_bit = 2.0f32.powf(self.prec-1.0);
		let mut d: bool = false;
		// set bit / target
		let t = if input < self.q || input == -1.0 {-1.0} else {1.0};
		if t > 0.0 {
			d = true;
		}
		// adjust charge
		let mut nq = self.q + ((self.s * (t-self.q) + prec_bit)*0.5f32.powf(self.prec));
		if nq == self.q && nq != t {
			nq += if t == 1.0 {1.0} else {-1.0};
		}
		self.q = nq;
		// adjust strength
		let st = if t != self.lt {0.0} else {prec_bit-1.0};
		let mut ns = self.s;
		if ns != st {
			ns += if st != 0.0 {1.0} else {-1.0};
		}
		if self.prec > 8.0 {
			let prec8_bit = 2.0f32.powf(self.prec-8.0)+1.0;
			if ns < prec8_bit {
				ns = prec8_bit;
			}
		}
		self.s = ns;
		self.lt = t;
		return d;
	}
}