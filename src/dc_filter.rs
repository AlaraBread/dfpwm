pub struct DCFilter {
	a: f32,
	k: f32,
	w: f32,
	z_1: f32,
	z_2: f32,
}

impl DCFilter {
	pub fn new() -> Self {
		let w: f32 = 0.0;
		let a: f32 = 0.9;
		let mut n = Self {
			a,
			k: 1.0,
			w,
			z_1: 0.0,
			z_2: 0.0,
		};
		n.set_a(a);
		return n;
	}

	pub fn set_a(&mut self, a: f32) {
		self.a = a;
		self.k = (1.0+2.0*a*self.w.cos()+a*a)/(2.0+2.0*self.w.cos());
	}

	pub fn reset(&mut self) {
		self.z_1 = 0.0;
		self.z_2 = 0.0;
	}

	pub fn process_sample(&mut self, input: f32) -> f32 {
		let out = input+self.z_1;
		self.z_1 = out*2.0*self.a*self.w.cos()-input*2.0*self.w.cos()+self.z_2;
		self.z_2 = input-out*self.a*self.a;
		return out*self.k;
	}
}