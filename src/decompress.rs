use crate::{dc_filter::DCFilter, notch_filter::NotchFilter};

pub struct DFPWMDecompress {
	fq: f32,
	q: f32,
	s: f32,
	lt: f32,
	pub dc_filter: DCFilter,
	pub notch_filter: NotchFilter,
	pub fs: f32, // postfilter
	pub prec: f32,
	pub dc_filter_enabled: bool,
	pub notch_filter_enabled: bool,
}

impl DFPWMDecompress {
	pub fn new() -> Self {
		Self {
			fq: 0.0,
			q: 0.0,
			s: 0.0,
			lt: -1.0,
			dc_filter: DCFilter::new(),
			notch_filter: NotchFilter::new(0.25),
			fs: 144.0,
			prec: 12.28,
			dc_filter_enabled: true,
			notch_filter_enabled: true,
		}
	}

	pub fn reset(&mut self) {
		self.fq = 0.0;
		self.q = 0.0;
		self.s = 0.0;
		self.lt = -1.0;
		self.dc_filter.reset();
		self.notch_filter.reset();
	}

	pub fn process_sample(&mut self, input: bool) -> f32{
		// set target
		let t = if input {1.0} else {-1.0};

		// adjust charge
		let nq = self.q + ((self.s * (t-self.q) + (2.0f32.powf(self.prec-1.0)))*0.5f32.powf(self.prec));
		if nq == self.q && nq != t {
			self.q += if t == 1.0 {1.0} else {-1.0};
		}
		let lq = self.q;
		self.q = nq;

		// adjust strength
		let st = if t != self.lt {0.0} else {(2.0f32.powf(self.prec))-1.0};
		let mut ns = self.s;
		if ns != st {
			ns += if st != 0.0 {1.0} else {-1.0};
		}
		if self.prec > 8.0 {
			let prec8_bit = 2.0f32.powf(self.prec-8.0)+1.0;
			if ns < prec8_bit {
				ns = prec8_bit;
			}
		}
		self.s = ns;

		// FILTER: perform antijerk
		let mut ov = if t != self.lt {(nq+lq)/2.0} else {nq};

		// FILTER: perform LPF
		self.fq += (self.fs*(ov-self.fq) + 128.0)*0.5f32.powf(8.0);
		ov = self.fq;

		self.lt = t;

		// FILTER: remove dc offset
		if self.dc_filter_enabled {
			ov = self.dc_filter.process_sample(ov);
		}

		// FILTER: remove whine
		if self.notch_filter_enabled {
			ov = self.notch_filter.process_sample(ov);
		}

		// output sample
		return ov;
	}
}