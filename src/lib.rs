pub mod notch_filter;
pub mod util;
pub mod dc_filter;
pub mod compress;
pub mod decompress;

use nih_plug::prelude::*;
use nih_plug_egui::egui::{Color32, Style};
use std::sync::Arc;
use nih_plug_egui::{EguiState, create_egui_editor, egui, widgets};
use crate::compress::DFPWMCompress;
use crate::decompress::DFPWMDecompress;
use util::*;

pub struct DFPWMPlugin {
	compressor: [DFPWMCompress; 2],
	decompressor: [DFPWMDecompress; 2],
	params: Arc<DFPWMPluginParams>,
}

#[derive(Params)]
struct DFPWMPluginParams {
	#[persist = "editor-state"]
    editor_state: Arc<EguiState>,
	#[id = "filter-constant"]
	pub filter_constant: FloatParam,
	#[id = "prec"]
	pub precision: FloatParam,
	#[id = "dc_filter"]
	pub dc_filter: BoolParam,
	#[id = "dc_filter_steepness"]
	pub dc_filter_steepness: FloatParam,
	#[id = "notch_filter"]
	pub notch_filter: BoolParam,
	#[id = "notch_filter_steepness"]
	pub notch_filter_steepness: FloatParam,
	#[id = "dry_wet"]
	pub dry_wet: FloatParam,
}

impl Default for DFPWMPlugin {
	fn default() -> Self {
		Self {
			compressor: [DFPWMCompress::new(), DFPWMCompress::new()],
			decompressor: [DFPWMDecompress::new(), DFPWMDecompress::new()],
			params: Arc::new(DFPWMPluginParams::default()),
		}
	}
}


pub fn v2s_bool() -> Arc<dyn Fn(bool) -> String + Send + Sync> {
    Arc::new(move |value| {
        if value {
            String::from("On")
        } else {
            String::from("Off")
        }
    })
}

pub fn s2v_bool() -> Arc<dyn Fn(&str) -> Option<bool> + Send + Sync> {
    Arc::new(|string| {
        let string = string.trim();
        if string.eq_ignore_ascii_case("on") {
            Some(true)
        } else if string.eq_ignore_ascii_case("off") {
            Some(false)
        } else {
            None
        }
    })
}


impl Default for DFPWMPluginParams {
	fn default() -> Self {
		Self {
			editor_state: EguiState::from_size(300, 500),

			filter_constant: FloatParam::new(
				"Filter Constant",
				144.0,
				FloatRange::Linear {
					min: 0.0,
					max: 500.0,
				},
			)
			.with_smoother(SmoothingStyle::Linear(50.0)),

			precision: FloatParam::new(
				"Precision",
				12.28,
				FloatRange::Skewed {
					min: 0.5,
					max: 16.0,
					factor: 0.5,
				},
			)
			.with_smoother(SmoothingStyle::Linear(50.0)),

			dry_wet: FloatParam::new(
				"Dry/Wet",
				1.0,
				FloatRange::Linear {
					min: 0.0,
					max: 1.0,
				},
			)
			.with_smoother(SmoothingStyle::Linear(50.0))
			.with_value_to_string(nih_plug::formatters::v2s_f32_percentage(1))
			.with_string_to_value(nih_plug::formatters::s2v_f32_percentage())
			.with_unit("%"),

			dc_filter: BoolParam::new("DC Filter", true)
			.with_string_to_value(s2v_bool())
			.with_value_to_string(v2s_bool()),

			dc_filter_steepness: FloatParam::new(
				"DC Filter Steepness",
				0.999,
				FloatRange::Skewed {
					min: 0.8, max: 0.9999, factor: 10.0
				}
			)
			.with_smoother(SmoothingStyle::Linear(50.0)),

			notch_filter: BoolParam::new("Notch Filter", true)
			.with_string_to_value(s2v_bool())
			.with_value_to_string(v2s_bool()),

			notch_filter_steepness: FloatParam::new(
				"Notch Filter Steepness",
				0.999,
				FloatRange::Skewed {
					min: 0.8, max: 0.9999, factor: 10.0
				}
			)
			.with_smoother(SmoothingStyle::Linear(50.0)),
		}
	}
}

impl Plugin for DFPWMPlugin {
	const NAME: &'static str = "DFPWM";
	const VENDOR: &'static str = "Alara Bread";
	const URL: &'static str = env!("CARGO_PKG_HOMEPAGE");
	const EMAIL: &'static str = "alara@alarabread.fun";

	const VERSION: &'static str = env!("CARGO_PKG_VERSION");

	const AUDIO_IO_LAYOUTS: &'static [AudioIOLayout] = &[AudioIOLayout {
		main_input_channels: NonZeroU32::new(2),
		main_output_channels: NonZeroU32::new(2),

		aux_input_ports: &[],
		aux_output_ports: &[],

		names: PortNames::const_default(),
	}];


	const MIDI_INPUT: MidiConfig = MidiConfig::None;
	const MIDI_OUTPUT: MidiConfig = MidiConfig::None;

	const SAMPLE_ACCURATE_AUTOMATION: bool = true;
	type SysExMessage = ();
	type BackgroundTask = ();

	fn params(&self) -> Arc<dyn Params> {
		self.params.clone()
	}

	fn editor(&mut self, _async_executor: AsyncExecutor<Self>) -> Option<Box<dyn Editor>> {
		let params = self.params.clone();
		create_egui_editor(self.params.editor_state.clone(), (), |_, _| {},
			move |egui_ctx, setter, _state| {
				egui::CentralPanel::default().show(egui_ctx, |ui| {

					let mut style = Style::default();
					style.visuals.widgets.noninteractive.bg_fill = Color32::from_rgb(73, 49, 16);
					style.visuals.override_text_color = Some(Color32::from_rgb(254, 236, 210));
					style.visuals.widgets.active.bg_fill = Color32::from_rgb(255, 193, 111);
					style.visuals.widgets.inactive.bg_fill = Color32::from_rgb(58, 36, 2);
					style.visuals.widgets.open.bg_fill = Color32::from_rgb(254, 236, 210);
					style.visuals.widgets.hovered.bg_fill = Color32::from_rgb(255, 193, 111);
					style.visuals.selection.bg_fill = Color32::from_rgb(254, 236, 210);

					egui_ctx.set_style(style);
					ui.label("Filter Constant");
                    ui.add(widgets::ParamSlider::for_param(&params.filter_constant, setter));
					ui.label("Precision");
                    ui.add(widgets::ParamSlider::for_param(&params.precision, setter));
					ui.label("DC Filter");
                    ui.add(widgets::ParamSlider::for_param(&params.dc_filter, setter));
					ui.label("DC Filter Steepness");
                    ui.add(widgets::ParamSlider::for_param(&params.dc_filter_steepness, setter));
					ui.label("Notch Filter");
                    ui.add(widgets::ParamSlider::for_param(&params.notch_filter, setter));
					ui.label("Notch Filter Steepness");
                    ui.add(widgets::ParamSlider::for_param(&params.notch_filter_steepness, setter));
					ui.label("Dry/Wet");
                    ui.add(widgets::ParamSlider::for_param(&params.dry_wet, setter));

					ui.separator();
					ui.separator();

					ui.label("This plugin was made by Alara Bread.");
					ui.separator();
					ui.label("The DFPWM algorithm was originally developed by by Ben \"GreaseMonkey\" Russell.");
				});
			}
		)
	}

	fn initialize(
		&mut self,
		_audio_io_layout: &AudioIOLayout,
		_buffer_config: &BufferConfig,
		_context: &mut impl InitContext<Self>,
	) -> bool {
		true
	}

	fn reset(&mut self) {
		for c in self.compressor.iter_mut() {
			(*c).reset();
		}
		for c in self.decompressor.iter_mut() {
			(*c).reset();
		}
	}

	fn process(
		&mut self,
		buffer: &mut Buffer,
		_aux: &mut AuxiliaryBuffers,
		_context: &mut impl ProcessContext<Self>,
	) -> ProcessStatus {
		for channel_samples in buffer.iter_samples() {
			let filter_constant = self.params.filter_constant.smoothed.next();
			let prec = self.params.precision.smoothed.next();
			let dry_wet = self.params.dry_wet.smoothed.next();
			let dc_filter = self.params.dc_filter.value();
			let dc_filter_steepness = self.params.dc_filter_steepness.smoothed.next();
			let notch_filter = self.params.notch_filter.value();
			let notch_filter_steepness = self.params.notch_filter_steepness.smoothed.next();
			
			for c in self.decompressor.iter_mut() {
				(*c).fs = filter_constant;
				(*c).prec = prec;
				(*c).dc_filter_enabled = dc_filter;
				(*c).dc_filter.set_a(dc_filter_steepness);
				(*c).notch_filter_enabled = notch_filter;
				(*c).notch_filter.set_a(notch_filter_steepness);
			}
			for c in self.compressor.iter_mut() {
				(*c).prec = prec;
			}
			let mut i = 0;
			for sample in channel_samples {
				let mut processed = self.decompressor[i].process_sample(self.compressor[i].process_sample(*sample));
				if (processed).is_infinite() || (processed).is_nan() {
					// extreme parameters can cause infinities or nans, reset if that happens
					processed = 0.0;
					self.reset();
				}
				*sample = lerp(*sample, processed, dry_wet);
				i += 1;
				i = i%2; // only support 2 channels for now
			}
		}

		ProcessStatus::Normal
	}
}

impl ClapPlugin for DFPWMPlugin {
	const CLAP_ID: &'static str = "fun.alarabread.dfpwm";
	const CLAP_DESCRIPTION: Option<&'static str> = Some("1 bit per sample audio codec");
	const CLAP_MANUAL_URL: Option<&'static str> = Some(Self::URL);
	const CLAP_SUPPORT_URL: Option<&'static str> = None;

	// Don't forget to change these features
	const CLAP_FEATURES: &'static [ClapFeature] = &[ClapFeature::AudioEffect, ClapFeature::Stereo];
}

impl Vst3Plugin for DFPWMPlugin {
	const VST3_CLASS_ID: [u8; 16] = *b"alaradfpwm      ";

	// And also don't forget to change these categories
	const VST3_SUBCATEGORIES: &'static [Vst3SubCategory] =
		&[Vst3SubCategory::Fx, Vst3SubCategory::Dynamics];
}

nih_export_clap!(DFPWMPlugin);
nih_export_vst3!(DFPWMPlugin);
